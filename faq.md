
----

Project
-------

organization
:   **methodology**

    scrum, safe, pmf, other

:   **state**

    mature, greenfield, other

:   **dependencies**

    standalone, interconnected

:   **delivery**

    periodical, rolling release

:   **release support**

    HEAD, tags


domain
:   **primary**

    telecom, embedded, cloud, other

:   **secondary**

    4G, 5G, other


language
:   **primary**

    cpp, c, rust, java, python, other

:   **secondary**

    cpp, c, rust, java, python, other

:   **cpp standard**

    latest, specified:

:   **standard library**

    allowed, limited

:   **thirdparty libraries**

    boost, absail, folly, other

:   **build system**

    autotools, cargo, cmake, meson, conan, waf, bazel, other

:   **compiler**

    gcc, clang, other

:   **serialization**

    protobuf, flatbuffers, json, spirit, serde


architecture
:   **goal**

    low latency, performance, throughput, scalability, maintainability, portability, power consumption, other

:   **concurrency**

    single threader, selectors, async, multithreading, multiprocessing, distributed, other

:   **state**

    stateless, statefull, microservices, other

:   **db**

    sql, sqlite, oracle, nosql, mongo, other

:   **os**

    posix, wrappers

:   **containers**

    docker, kubernetes, azure, swarm, lxc, other

----

Development environment
-----------------------

repository
:   **type**

    git, svn, other

:   **host**

     github, gitlab, gerrit, gogs, other

:   **infrastructure**

    external, internal

:   **branching model**

    git-flow, other


system
:   **operating system**

    windows, linux distr, other

:   **toolchain**

    vanilla, vendor

:   **packaging system**

    yocto, deb, rpm, pkgbuild, other

:   **target**

    native build, cross-compilation

:   **build**

    local, remote

----

Continuous Integration
----------------------

quality
:   **coding standard**

    core guidelines, domain specific guidelines

:   **CI**

    traivs, gitlab-runner, buildbot, drone, other

:   **quality metrics**

    code coverage: lines, code coverage: branches, static analysis, cyclomatic complexity analysis, other

:   **unittesting**

    gtest, gmock, catch, other

:   **system testing**

    pytest, other

:   **acceptance tests**

    behave, cucumber, other

:   **performance testing**

    google benchmar, profilers, gprof, other

:   **pentesting**

    libFuzzer, AFL, other

:   **ticketing system**

    github, gitlab, jira, other

:   **bugtacker**

    github, gitlab, bugzilla, other:

:   **specification**

    rfc, doors, other

:   **user manual**

    sphinx, rtd, man pages, other

:   **dev reference**

    doxygen, docstring, other:

:   **wiki**

    gitlab, github, confluence

----

Devstation
----------

local
:   **machine**

    laptop, pc, thin client, other

:   **cpu**

    4, 6, 8, other

:   **ram**

    8, 16, 32, other

:   **network**

    opened, restricted

:   **communication**

    slack, hangouts, zoom, other
