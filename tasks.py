from invoke import task, Context
from glob import glob


@task
def clean(ctx, with_result=False):
    ctx.run(f'rm -f ./*.log ./*.tuc ./*.tex')
    if with_result:
        ctx.run(f'rm -f ./*.pdf')


@task(post=[clean])
def build(ctx, in_docker=True):
    style = 'styles/chmduquesne.tex'
    output = f'faq.tex'
    result = f'faq.pdf'
    cmd = f'pandoc --standalone --template {style} --from markdown --to context --variable papersize=A4 --output {output} faq.md'
    ctx.run(cmd.strip())
    ctx.run(f'context {output} --purgeall --result={result}'.strip(), hide='out')
    cmd = f'pandoc --standalone --from markdown --to pdf --variable papersize=A4 -t latex --output checkboxes.pdf checkboxes.md'
    ctx.run(cmd.strip())
